package controllers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"log"
	"strconv"
)

func isError(errarg error,errmsg string, w http.ResponseWriter) bool{

	if errarg != nil {

		err := ErrorResponse{
			Err: errmsg,
		}
		json.NewEncoder(w).Encode(err)
		return true
	}

	return false

}

func DispError(err error, errMsg string) error {

	if err!=nil{
		log.Printf("Task Failed!"+errMsg)

		return err
	}
	log.Printf("Task Successfull!"+errMsg)
	return nil
}

func GetId(idName string, r *http.Request) uint{

	params := mux.Vars(r)
	id:=params[idName]
	parsedId,_:= strconv.ParseInt(id,10,32)
	return uint(parsedId)

}



