package controllers

import (

	"auth/models"
	"auth/utils"
	"encoding/json"
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"os"
	"time"
	"strconv"
)

//Exception struct
type Exception models.Exception

type ErrorResponse struct {
	Err string
}

type error interface {
	Error() string
}

var db = utils.ConnectDB()

func MagaAPI(w http.ResponseWriter, r *http.Request) {
	fmt.Println("i am here")
}

func TestAPI(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("API live and kicking"))
}

func Login(w http.ResponseWriter, r *http.Request) {
	user := &models.UserInfo{}
	err := json.NewDecoder(r.Body).Decode(&user)

	if isError(err,"Invalid Login Request",w){
		return
	}
	FindOne(user.Email, user.Password,w) //get jwt based token

}

func FindOne(email, password string, w http.ResponseWriter) {

	user := &models.UserInfo{}
	err := db.Where("Email = ?", email).First(&user).Error

	if isError(err,"Invalid Login Request",w){
		return
	}

	expiresAt := time.Now().Add(time.Hour * 72).Unix()

	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if errf != nil && errf == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		var resp = map[string]interface{}{"status": false, "message": "Invalid login credentials. Please try again"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	tk := &models.Token{
		UserID: user.UserId,
		Email:  user.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	//Load environmenatal variables
	//godotenv.Load()
	jwtSecret := os.Getenv("jwtSecret")


	tokenString, error := token.SignedString([]byte(jwtSecret))

	if isError(error,"Failed To JWT Token",w){
		return
	}

	var resp = map[string]interface{}{"message": "Login Authenticated -  JWT Token Assigned!"}
	resp["token"] = tokenString //Store the token in the response
	resp["email"] = user.Email
	json.NewEncoder(w).Encode(resp)
}

//CreateUser function -- create a new user
func CreateUser(w http.ResponseWriter, r *http.Request) {

	user := &models.UserInfo{}
	json.NewDecoder(r.Body).Decode(&user)
	//fmt.Println("Extracted: ",user.Password,user.Email)
	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

	if isError(err,"Password Encryption Failed!",w){
		return
	}

	user.Password = string(pass)
	createdUser := db.Create(user)

	if isError(createdUser.Error,"Couldnot log user in db!",w){
		return
	}
	json.NewEncoder(w).Encode(&createdUser)
}

//FetchUser function
func FetchUsers(w http.ResponseWriter, r *http.Request) {
	var users []models.User
	db.Preload("auths").Find(&users)

	json.NewEncoder(w).Encode(users)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	params := mux.Vars(r)
	var id = params["id"]
	db.First(&user, id)
	json.NewDecoder(r.Body).Decode(user)
	db.Save(&user)
	json.NewEncoder(w).Encode(&user)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var id = params["id"]
	var user models.User
	db.First(&user, id)
	db.Delete(&user)
	json.NewEncoder(w).Encode("User deleted")
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var id = params["id"]
	var user models.User
	db.First(&user, id)
	json.NewEncoder(w).Encode(&user)
}


func AddTodo(w http.ResponseWriter, r *http.Request) {

	todo := &models.ToDo{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&todo)

	if isError(err,"Payload is Required!",w){
		return
	}

	if todo.Title == "" {
		if isError(err,"Title is Required!",w){
			return
		}
	}

	//insert task in db
	params := mux.Vars(r)
	id:=params["id"]
	//fmt.Println("ID IS : ",id)
	parsedId,err:= strconv.ParseInt(id,10,32)
	todo.UserId = uint(parsedId)
	//fmt.Println("ID FETCHED",parsedId,to;do.UserId)
	createdToDo := db.Create(todo)
	if isError(createdToDo.Error,"Task Addition Failed!",w){
		return
	}
	//fmt.Println("HERE")
	json.NewEncoder(w).Encode(createdToDo)
}

func UpdateTodo(w http.ResponseWriter, r *http.Request) {
	todo := &models.ToDo{}
	json.NewDecoder(r.Body).Decode(&todo)
	userId:=GetId("id",r)
	taskId:=GetId("taskid",r)
	todo.UserId=userId
	todo.UserTaskId=taskId
	fmt.Println("Content ",todo.Content," Title ",todo.Title)
	db.Model(&todo).Where(models.ToDo{UserId:todo.UserId,UserTaskId: todo.UserTaskId}).Updates(models.ToDo{Title:todo.Title, Content:todo.Content})
}

func DeleteTodo(w http.ResponseWriter, r *http.Request) {

	userId:=GetId("id",r)
	taskId:=GetId("taskid",r)
	db.Where(models.ToDo{UserId: userId,UserTaskId: taskId}).Delete(&models.ToDo{})
}

func ViewTodos(w http.ResponseWriter, r *http.Request) {

	userId:=GetId("id",r)
	var todos []models.ToDo
	db.Where(models.ToDo{UserId:userId}).Find(&todos)
	json.NewEncoder(w).Encode(&todos)
}

