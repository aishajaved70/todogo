package routes

import (
	"auth/controllers"
	"auth/utils/auth"
	"github.com/gorilla/mux"
	"net/http"
)

func Handlers() *mux.Router {

	r := mux.NewRouter().StrictSlash(true)
	r.Use(CommonMiddleware)

	//r.HandleFunc("/", controllers.TestAPI).Methods("GET")
	//r.HandleFunc("/api", controllers.TestAPI).Methods("GET")
	r.HandleFunc("/register", controllers.CreateUser).Methods("POST")
	r.HandleFunc("/login", controllers.Login).Methods("POST")

	// Auth route
	s := r.PathPrefix("/auth").Subrouter()
	s.Use(auth.JwtVerify)
	//s.HandleFunc("/user", controllers.FetchUsers).Methods("GET")
	//s.HandleFunc("/addTask", controllers.AddTodo).Methods("POST")
	s.HandleFunc("/addTodo/{id}", controllers.AddTodo).Methods("POST")
	s.HandleFunc("/updateTodo/{id}/{taskid}", controllers.UpdateTodo).Methods("PUT")
	s.HandleFunc("/deleteTodo/{id}/{taskid}", controllers.DeleteTodo).Methods("DELETE")
	s.HandleFunc("/viewTodos/{id}", controllers.ViewTodos).Methods("GET")
	return r
}

// CommonMiddleware --Set content-type
func CommonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		//w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		//w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")
		next.ServeHTTP(w, r)
	})
}
