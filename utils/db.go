package utils

import (

	"auth/models"
	"fmt"

	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //Gorm postgres dialect interface
	"github.com/joho/godotenv"
)

//ConnectDB function: Make database connection
func ConnectDB() *gorm.DB {

	//Load environmenatal variables
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	username := os.Getenv("databaseUser")
	password := os.Getenv("databasePassword")
	databaseName := os.Getenv("databaseName")
	databaseHost := os.Getenv("databaseHost")

	//Define DB connection string
	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", databaseHost, username, databaseName, password)

	//connect to db URI
	db, err := gorm.Open("postgres", dbURI)

	if err != nil {
		fmt.Println("error", err)
		panic(err)
	}
	// close db when not in use
	// defer db.Close()

	// Migrate the schema
	/*db.AutoMigrate(
		&models.User{})*/
	fmt.Println("db connection successful")
	///db.CreateTable(&models.UserInfo{})
	//db.CreateTable(&models.TolDo{})

	db.AutoMigrate(&models.ToDo{}, &models.UserInfo{})
	db.Model(&models.ToDo{}).AddForeignKey("user_id", "User_Infos(user_id)", "RESTRICT", "RESTRICT")

	return db
}



/*
import (
	"auth/controllers"
	"auth/models"
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"

	"log"
	"os"

	_ "github.com/jinzhu/gorm/dialects/postgres" //Gorm postgres dialect interface
	"github.com/joho/godotenv"
)

func Connect(){ //main db connection function

	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	var dbConn *pg.DB

	username := os.Getenv("databaseUser")
	password := os.Getenv("databasePassword")
	databasePort := os.Getenv("databasePort")
	databaseHost := os.Getenv("databaseHost")

	var opts *pg.Options=&pg.Options{
		User:username,
		Password:password,
		Addr: databaseHost+":"+databasePort,
	}

	dbConn=pg.Connect(opts)
	fmt.Println(dbConn)

	if dbConn==nil{
		log.Printf("Database Connection Failed")
		return
	}
	log.Printf("Database Connection Successful")
	CreateUserInfoTable(dbConn)

}//Connect ends

/*
func closeDb(){ //main db connection function

	closeErr:=dbConn.Close()
	if closeErr!=nil{
		fmt.Println("db connection close FAILED")
		os.Exit(404)
	}

	fmt.Println("db connection closed successfully")

}//Connect ends
*/


/*
func CreateUserInfoTable(db *pg.DB) error {

	optsPtr :=&orm.CreateTableOptions{
		IfNotExists:true, //only create table if it doesnot exist before
	}

	var userInfoPtr *models.UserInfo=&models.UserInfo{} //empty struct
	var createErr error=db.CreateTable(userInfoPtr, optsPtr)
	return controllers.DispError(createErr,"DSBook Table Creation")
}*/

/*
func InsertTable(dsbook_obj DSBook)error{
	return InsertDSBookTable(dbConn,dsbook_obj)
}


func ReadTable() []DSBook{
	return ReadDSBooks(dbConn)
}

func UpdateTable(dsbook_obj DSBook)error{
	return UpdateDSBookTable(dbConn,dsbook_obj)
}

func DelTable(dsbook_obj DSBook)error{
	return DelDSBookTable(dbConn,dsbook_obj)
}
*/