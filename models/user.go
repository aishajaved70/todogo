package models

import (

	"github.com/jinzhu/gorm"
)

//User struct declaration
type User struct {

	gorm.Model
	Email    string `gorm:"type:varchar(100);unique_index"`
	Password string `json:"Password"`
}

type UserInfo struct {

	UserId uint `gorm:"primary_key"`
	Email    string `gorm:"type:varchar(100);unique;not null"`
	Password string `json:"Password,not null"`

}

type ToDo struct {

	UserTaskId uint `gorm:"primary_key"`
	Title   string `gorm:"type:varchar(100)not null"`
	Content string `gorm:"type:varchar(100);not null"`
	UserId  uint //fk from UserInfo
}